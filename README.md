# ATELIER README

## To run the project you will need

  1. [Docker](https://www.docker.com/get-started)
  1. [Ruby v.3.2.1 with rbenv](https://github.com/rbenv/rbenv)
  1. [Rails 7.0.4.2](https://rubyonrails.org/2023/1/24/Rails-7-0-4-2-and-6-1-7-2-have-been-released)
  1. [Postgres@14](https://www.postgresql.org/about/news/postgresql-14-released-2318/)
  1. [Redis](https://redis.io/)

## Instruction to run atelier application

### Clone atelier repository

  To clone atelier repository please run one of the commands bellow:

  ``` bash
  git clone git@gitlab.com:rails_docker/atelier.git
  ```

  or

  ``` bash
  git clone https://gitlab.com/rails_docker/atelier.git
  ```

### Add docker volume and alias

  In the docker volume, we will store our bundle data and the alias is just a way to avoid writing too many instructions after each command.
  
  Before execute next commands please make sure that docker is running!

  ``` docker
  docker volume create ruby-bundle-cache
  ```

  ``` bash
  alias docked='docker run --rm -it -v ${PWD}:/rails -v ruby-bundle-cache:/bundle ghcr.io/rails/cli'
  ```

### Build and run atelier project with docker

  ``` docker
  # 1. Build
  docker-compose build

  # 2. Install
  docked bundle install

  # 3. Initialize
  docker-compose up

  # 4. Run
  # Execute next command in your NEW tab of the terminal
  docker-compose run web rails db:create
  ```

  Open [localhost:3000](http://localhost:3000) in your browser and enjoy.
